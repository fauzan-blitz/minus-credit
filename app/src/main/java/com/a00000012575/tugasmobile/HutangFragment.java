package com.a00000012575.tugasmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class HutangFragment extends Fragment {
    SharedPreferences sharedPreferences;
    public static final String myPreferences = "userPreferences";
    ProgressDialog dialog;
    ListView lv;
    ArrayList<HashMap<String,String>> list_hutang;
    String get_list_hutang = "http://api.surprise-stories.com/minus-credit/getlisthutang.php";
    JSONParser jsonParser = new JSONParser();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hutang, container, false);

        sharedPreferences = getContext().getSharedPreferences(myPreferences, Context.MODE_PRIVATE);
        tampilhutangfunction();

        list_hutang = new ArrayList<HashMap<String, String>>();
        lv = (ListView) view.findViewById(R.id.listHutang);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                HashMap<String,Object> obj = (HashMap<String, Object>) lv.getAdapter().getItem(position);
                Log.d("OBJ",obj.toString());

                String get_id = (String) obj.get("idHutang");
                Intent i = new Intent(getContext(),DetailHutang.class);
                i.putExtra("idHutang",get_id);
                startActivityForResult(i,100);
            }
        });
        return view;
    }

    private void tampilhutangfunction() {
        final String[] message = new String[1];

        class TampilHutang extends AsyncTask<String,String,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(getContext());
                dialog.setMessage("Loading data hutang");
                dialog.setCancelable(true);
                dialog.setIndeterminate(false);
                dialog.show();
            }

            @Override
            protected String doInBackground(String... strings) {

                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("user_id",Integer.toString(sharedPreferences.getInt("idUser",0))));

                JSONObject json = jsonParser.makeHttpRequest(get_list_hutang,"POST",param);

                try{
                    int sukses = json.getInt("success");

                    if(sukses == 1) {
                        JSONArray hutangAr = json.getJSONArray("data");

                        if(hutangAr.length()>0) {
                            for(int i = 0; i < hutangAr.length(); i++) {
                                JSONObject a = hutangAr.getJSONObject(i);
                                String idHutang = a.getString("idHutang");
                                String total_debit = "Rp " + a.getString("total_debit");
                                String due_date = a.getString("due_date");
                                String kreditor = a.getString("kreditor");

                                HashMap<String, String> map = new HashMap<>();
                                map.put("idHutang",idHutang);
                                map.put("total_debit",total_debit);
                                map.put("due_date",due_date);
                                map.put("kreditor",kreditor);
                                list_hutang.add(map);
                            }
                        }else{
                            message[0] = "hutang kosong";
                        }
                    }else{
                        message[0] = json.getString("message");
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
                return message[0];
            }

            @Override
            protected void onPostExecute(String s) {
                if(s != "hutang kosong") {
                    ListAdapter adapter = new SimpleAdapter(getContext(),list_hutang,R.layout.list_hutang, new String[] {"idHutang","total_debit","due_date","kreditor"}, new int[] {R.id.txtIdHutang,R.id.textTotalDebit,R.id.textDueDate,R.id.textKreditor});
                    lv.setAdapter(adapter);
                }
                dialog.dismiss();
                super.onPostExecute(s);
            }
        }

        TampilHutang tampilHutang = new TampilHutang();
        tampilHutang.execute();
    }

}
