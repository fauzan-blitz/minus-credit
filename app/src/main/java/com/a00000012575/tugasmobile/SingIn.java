package com.a00000012575.tugasmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.a00000012575.tugasmobile.R.styleable.View;

public class SingIn extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    public static final String myPreferences = "userPreferences";
    ProgressDialog dialog;
    JSONParser jsonParser = new JSONParser();
    EditText username, password;
    Button signIn;
    String api_login = "http://api.surprise-stories.com/minus-credit/signin.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_in);

        sharedPreferences = getApplicationContext().getSharedPreferences(myPreferences,Context.MODE_PRIVATE);

        if(sharedPreferences.getInt("idUser",0) != 0) {
            startActivity(new Intent(SingIn.this,MainActivity.class));
            finish();
        }

        username = (EditText) findViewById(R.id.edtUsername);
        password = (EditText) findViewById(R.id.edtPassword);
        signIn = (Button) findViewById(R.id.btnSignIn);

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

    }

    public void Register(View v){
        TextView register = (TextView)findViewById(R.id.SignUp2);
            Intent intentRegister = new Intent(getApplicationContext(),Register.class);
            startActivity(intentRegister);
    }

    private void login() {
        final String[] message = new String[1];

        class Login extends AsyncTask<String,String,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(SingIn.this);
                dialog.setMessage("Proses Login");
                dialog.setIndeterminate(false);
                dialog.setCancelable(true);
                dialog.show();
            }

            @Override
            protected String doInBackground(String... strings) {
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("username",username.getText().toString()));
                param.add(new BasicNameValuePair("password",password.getText().toString()));

                JSONObject json = jsonParser.makeHttpRequest(api_login,"POST",param);

                try{
                    int sukses = json.getInt("success");

                    if(sukses == 1) {
                        JSONObject user = json.getJSONObject("user");
                        sharedPreferences = getApplicationContext().getSharedPreferences(myPreferences, Context.MODE_PRIVATE);
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putInt("idUser",user.getInt("id"));
                        edit.putString("username",user.getString("username"));
                        edit.commit();
                        message[0] = "Login";
                    }else{
                        message[0] = json.getString("message");
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }

                return message[0];
            }

            @Override
            protected void onPostExecute(String s) {
                if(s == "Login") {
                    startActivity(new Intent(SingIn.this,MainActivity.class));
                    finish();
                }
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_SHORT).show();
                super.onPostExecute(s);
            }
        }

        Login login = new Login();
        login.execute();
    }
}
