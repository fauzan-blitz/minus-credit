package com.a00000012575.tugasmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.R.id.message;

public class TambahPiutangFragment extends Fragment {
    SharedPreferences sharedPreferences;
    public static final String myPreferences = "userPreferences";
    EditText edtDueDate, edtDescription, edtDebitor, edtTotalDebit;
    Button btnAdd;
    ProgressDialog dialog;
    String add_receivable = "http://api.surprise-stories.com/minus-credit/addreceivable.php";
    JSONParser jsonParser = new JSONParser();
    String duedate, description, totaldebit, debitor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tambah_piutang, container, false);


        edtDueDate = (EditText) view.findViewById(R.id.edtDueDate);
        edtDebitor = (EditText) view.findViewById(R.id.edtDebitor);
        edtDescription = (EditText) view.findViewById(R.id.edtDescription);
        edtTotalDebit = (EditText) view.findViewById(R.id.edtTotalDebit);

        btnAdd = (Button) view.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferences = getContext().getSharedPreferences(myPreferences, Context.MODE_PRIVATE);
                tambahpiutangfunction();
            }
        });
        return view;
    }

    private void tambahpiutangfunction() {
        final String[] message = new String[1];

        class tambahpiutang extends AsyncTask<String, String, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(getContext());
                dialog.setMessage("Proses Menambahkan Piutang");
                dialog.setIndeterminate(false);
                dialog.setCancelable(true);
                dialog.show();

                duedate = edtDueDate.getText().toString();
                description = edtDescription.getText().toString();
                debitor = edtDebitor.getText().toString();
                totaldebit = edtTotalDebit.getText().toString();

            }


            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("due_date", duedate));
                param.add(new BasicNameValuePair("description", description));
                param.add(new BasicNameValuePair("debitor", debitor));
                param.add(new BasicNameValuePair("total_debit", totaldebit));
                param.add(new BasicNameValuePair("user_id", Integer.toString(sharedPreferences.getInt("idUser",0))));

                JSONObject json = jsonParser.makeHttpRequest(add_receivable, "POST", param);

                try {
                    int sukses = json.getInt("success");
                    if (sukses == 1) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                edtDueDate.setText("");
                                edtDescription.setText("");
                                edtDebitor.setText("");
                                edtTotalDebit.setText("");
                            }
                        });
                        message[0] = json.getString("message");
                    } else {
                        Toast.makeText(getContext(), json.getString("Proses menambahkan piutang gagal"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return message[0];

            }

            @Override
            protected void onPostExecute(String s) {
                dialog.dismiss();
                Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show();
                super.onPostExecute(s);
            }
        }
        tambahpiutang addpiutang = new tambahpiutang();
        addpiutang.execute();
    }
}