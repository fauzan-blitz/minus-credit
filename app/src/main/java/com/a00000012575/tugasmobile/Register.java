package com.a00000012575.tugasmobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Register extends AppCompatActivity {

    ProgressDialog dialog;
    EditText name, username, password, repassword, email;
    Button signup;
    JSONParser jsonParser = new JSONParser();
    String api_register = "http://api.surprise-stories.com/minus-credit/register.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        signup = (Button)findViewById(R.id.btnSignup);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intentsu = new Intent(Register.this, MainActivity.class);
//                startActivity(intentsu);

                name = (EditText) findViewById(R.id.name);
                username = (EditText) findViewById(R.id.username);
                password = (EditText) findViewById(R.id.password);
                repassword = (EditText) findViewById(R.id.repassword);
                email = (EditText) findViewById(R.id.email);
                signup = (Button) findViewById(R.id.btnSignup);
//                Toast.makeText(getApplicationContext(),"keisi semua",Toast.LENGTH_SHORT).show();

                if (name.getText().toString().length() == 0) {
                    name.setError("Please enter your name");
                    name.requestFocus();
                }else{
                    if (username.getText().toString().length() == 0) {
                        username.setError("Username is required");
                        username.requestFocus();
                    }else{
                        if (email.getText().toString().length() == 0) {
                            email.setError("Required!");
                            email.requestFocus();
                        }else{
                            if (password.getText().toString().length() == 0) {
                                password.setError("Password not entered");
                                password.requestFocus();
                            }else{
                                if (repassword.getText().toString().length() == 0) {
                                    repassword.setError("Please confirm password");
                                    repassword.requestFocus();
                                }else{
                                    if (!password.getText().toString().equals(repassword.getText().toString())){
                                        repassword.setError("Password not match!");
                                        repassword.requestFocus();
                                    }else{
                                        if (password.getText().toString().length()<6 ) {
                                            password.setError("Password should be at least 6 characters");
                                            password.requestFocus();
                                        }else{
                                            registrasi();
//                                            new Registrasi().execute();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    private void registrasi() {
        final String[] message = new String[1];

        class Registrasi extends AsyncTask<String,String,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(Register.this);
                dialog.setMessage("Proses mendaftar");
                dialog.setIndeterminate(false);
                dialog.setCancelable(true);
                dialog.show();
//            Toast.makeText(getApplicationContext(),"pre execute",Toast.LENGTH_SHORT).show();
            }

            @Override
            protected String doInBackground(String... strings) {
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("username",username.getText().toString()));
                param.add(new BasicNameValuePair("name",name.getText().toString()));
                param.add(new BasicNameValuePair("password",password.getText().toString()));
                param.add(new BasicNameValuePair("repassword",repassword.getText().toString()));
                param.add(new BasicNameValuePair("email",email.getText().toString()));

                JSONObject json = jsonParser.makeHttpRequest(api_register, "POST", param);

                try {
                    int sukses = json.getInt("success");
//                Log.d("regis api",new Integer(sukses).toString());
                    if(sukses == 1) {
                        message[0] = json.getString("message");
                        Intent intentRegister = new Intent(Register.this, SingIn.class);
                        startActivity(intentRegister);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(),json.getString("message"),Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
                return message[0];
            }

            //        @Override
            protected void onPostExecute(String s) {
//            Toast.makeText(getApplicationContext(),json.getString("message"),Toast.LENGTH_LONG).show();
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
                super.onPostExecute(s);
//            startActivity(new Intent(getApplicationContext(), SingIn.class));
//            finish();
            }
        }
        Registrasi regis = new Registrasi();
        regis.execute();
    }

    public void Signin(View view) {
        TextView Singin = (TextView) findViewById(R.id.Signin2);
        Intent intentRegister = new Intent(Register.this, SingIn.class);
        startActivity(intentRegister);
    }
}