package com.a00000012575.tugasmobile;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    HomeFragment homeFragment = new HomeFragment();
    HutangFragment hutangFragment = new HutangFragment();
    SettingFragment settingFragment = new SettingFragment();
    TambahPiutangFragment tambahPiutangFragment = new TambahPiutangFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.Frame,homeFragment);
        fragmentTransaction.commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_home:
//                    Toast.makeText(getApplicationContext(),"Home",Toast.LENGTH_SHORT).show();
                    homeFragment.setArguments(getIntent().getExtras());
                    fragmentTransaction.replace(R.id.Frame,homeFragment);
                    fragmentTransaction.commit();
                    return true;
                case R.id.navigation_transaction:
//                    Toast.makeText(getApplicationContext(),"Hutang",Toast.LENGTH_SHORT).show();
                    hutangFragment.setArguments(getIntent().getExtras());
                    fragmentTransaction.replace(R.id.Frame,hutangFragment);
                    fragmentTransaction.commit();
                    return true;
                case R.id.navigation_add:
                    tambahPiutangFragment.setArguments(getIntent().getExtras());
                    fragmentTransaction.replace(R.id.Frame,tambahPiutangFragment);
                    fragmentTransaction.commit();
                    return true;
                case R.id.navigation_settings:
                    settingFragment.setArguments(getIntent().getExtras());
                    fragmentTransaction.replace(R.id.Frame,settingFragment);
                    fragmentTransaction.commit();
                    return true;
            }
            return false;
        }



    };
}
