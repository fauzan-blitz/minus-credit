package com.a00000012575.tugasmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailHutang extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    public static final String myPreferences = "userPreferences";
    ProgressDialog dialog;
    String get_detail_piutang = "http://api.surprise-stories.com/minus-credit/payabledetail.php";
    JSONParser jsonParser = new JSONParser();
    String idPiutang;
    TextView txtIdPiutang, txtDueDate, txtDebitor, txtTotalPiutang, txtStatus, txtDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_hutang);

        sharedPreferences = getApplicationContext().getSharedPreferences(myPreferences, Context.MODE_PRIVATE);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        idPiutang = i.getStringExtra("idHutang");

        txtIdPiutang = (TextView)findViewById(R.id.txtIdPiutang);
        txtDueDate = (TextView)findViewById(R.id.txtDueDate);
        txtDebitor = (TextView)findViewById(R.id.txtDebitor);
        txtTotalPiutang = (TextView)findViewById(R.id.txtTotalPiutang);
        txtStatus = (TextView)findViewById(R.id.txtStatus);
        txtDescription = (TextView)findViewById(R.id.txtDescription);
        txtIdPiutang.setText(idPiutang);

        detailPiutangFunction();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void detailPiutangFunction() {
        final String[] message = new String[1];

        class DetailPiutangAPI extends AsyncTask<String,String,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(DetailHutang.this);
                dialog.setMessage("Loading detail hutang");
                dialog.setCancelable(true);
                dialog.setIndeterminate(false);
                dialog.show();
            }

            @Override
            protected String doInBackground(String... strings) {

                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("user_id",Integer.toString(sharedPreferences.getInt("idUser",0))));
                param.add(new BasicNameValuePair("receivable_id",idPiutang));

                JSONObject json = jsonParser.makeHttpRequest(get_detail_piutang,"POST",param);

                try{
                    int sukses = json.getInt("success");

                    if(sukses == 1) {
                        JSONArray data = json.getJSONArray("data");
                        final JSONObject isiData = data.getJSONObject(0);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    txtDueDate.setText(isiData.getString("due_date"));
                                    txtDebitor.setText(isiData.getString("debitor"));
                                    txtTotalPiutang.setText("Rp "+isiData.getString("total_debit"));
                                    txtStatus.setText(isiData.getString("status"));
                                    txtDescription.setText(isiData.getString("description"));

                                    message[0] = "sukses";
                                }catch(JSONException e){
                                    e.printStackTrace();
                                }
                            }
                        });
                    }else{
                        message[0] = json.getString("message");
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
                return message[0];
            }

            @Override
            protected void onPostExecute(String s) {
                dialog.dismiss();
                super.onPostExecute(s);
            }
        }

        DetailPiutangAPI detailPiutangAPI = new DetailPiutangAPI();
        detailPiutangAPI.execute();
    }
}
