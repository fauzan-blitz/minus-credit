package com.a00000012575.tugasmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DetailPiutang extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    public static final String myPreferences = "userPreferences";
    ProgressDialog dialog;
    String get_detail_piutang = "http://api.surprise-stories.com/minus-credit/receivabledetail.php";
    String lunasin_hutang_api = "http://api.surprise-stories.com/minus-credit/piutanglunas.php";
    JSONParser jsonParser = new JSONParser();
    String idPiutang;
    TextView txtIdPiutang, txtDueDate, txtDebitor, txtTotalPiutang, txtStatus, txtDescription;
    Button btnKonfirmasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_piutang);

        sharedPreferences = getApplicationContext().getSharedPreferences(myPreferences, Context.MODE_PRIVATE);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        idPiutang = i.getStringExtra("idPiutang");

        btnKonfirmasi = (Button)findViewById(R.id.btnLunas);

        txtIdPiutang = (TextView)findViewById(R.id.txtIdPiutang);
        txtDueDate = (TextView)findViewById(R.id.txtDueDate);
        txtDebitor = (TextView)findViewById(R.id.txtDebitor);
        txtTotalPiutang = (TextView)findViewById(R.id.txtTotalPiutang);
        txtStatus = (TextView)findViewById(R.id.txtStatus);
        txtDescription = (TextView)findViewById(R.id.txtDescription);
        txtIdPiutang.setText(idPiutang);
        detailPiutangFunction();

        btnKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lunasinPiutangFunction();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void lunasinPiutangFunction() {
        final String[] message = new String[1];

        class lunasinAPI extends AsyncTask<String,String,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(DetailPiutang.this);
                dialog.setMessage("Loading detail piutang");
                dialog.setCancelable(true);
                dialog.setIndeterminate(false);
                dialog.show();
            }

            @Override
            protected String doInBackground(String... strings) {

                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("user_id",Integer.toString(sharedPreferences.getInt("idUser",0))));
                param.add(new BasicNameValuePair("receivable_id",idPiutang));

                JSONObject json = jsonParser.makeHttpRequest(lunasin_hutang_api,"POST",param);

                try{
                    int sukses = json.getInt("success");

                    if(sukses == 1) {
                        message[0] = json.getString("message");
                        Intent i = new Intent(getApplicationContext(),MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }else{
                        message[0] = "Gagal Lunas";
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
                return message[0];
            }

            @Override
            protected void onPostExecute(String s) {
                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                super.onPostExecute(s);
            }
        }

        lunasinAPI lunasinAPI = new lunasinAPI();
        lunasinAPI.execute();
    }

    private void detailPiutangFunction() {
        final String[] message = new String[1];

        class DetailPiutangAPI extends AsyncTask<String,String,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(DetailPiutang.this);
                dialog.setMessage("Loading detail piutang");
                dialog.setCancelable(true);
                dialog.setIndeterminate(false);
                dialog.show();
            }

            @Override
            protected String doInBackground(String... strings) {

                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("user_id",Integer.toString(sharedPreferences.getInt("idUser",0))));
                param.add(new BasicNameValuePair("receivable_id",idPiutang));

                JSONObject json = jsonParser.makeHttpRequest(get_detail_piutang,"POST",param);

                try{
                    int sukses = json.getInt("success");

                    if(sukses == 1) {
                        JSONArray data = json.getJSONArray("data");
                        final JSONObject isiData = data.getJSONObject(0);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    txtDueDate.setText(isiData.getString("due_date"));
                                    txtDebitor.setText(isiData.getString("debitor"));
                                    txtTotalPiutang.setText("Rp "+isiData.getString("total_debit"));
                                    txtStatus.setText(isiData.getString("status"));
                                    txtDescription.setText(isiData.getString("description"));

                                    if((isiData.getString("status")).equals("LUNAS")) {
                                        btnKonfirmasi.setEnabled(false);
                                        btnKonfirmasi.setClickable(false);
                                        btnKonfirmasi.setVisibility(View.GONE);
                                    }
                                    message[0] = "sukses";
                                }catch(JSONException e){
                                    e.printStackTrace();
                                }
                            }
                        });
                    }else{
                        message[0] = json.getString("message");
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
                return message[0];
            }

            @Override
            protected void onPostExecute(String s) {
                dialog.dismiss();
                super.onPostExecute(s);
            }
        }

        DetailPiutangAPI detailPiutangAPI = new DetailPiutangAPI();
        detailPiutangAPI.execute();
    }
}
