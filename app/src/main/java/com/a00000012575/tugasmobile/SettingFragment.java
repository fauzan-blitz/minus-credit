package com.a00000012575.tugasmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SettingFragment extends Fragment {
    SharedPreferences sharedPreferences;
    public static final String myPreferences = "userPreferences";
    EditText userName, name, email, phone;
    ProgressDialog dialog;
    String get_profile = "http://api.surprise-stories.com/minus-credit/getprofile.php";
    String update_profile = "http://api.surprise-stories.com/minus-credit/editprofile.php";
    JSONParser jsonParser = new JSONParser();
    Button save, btnLogout;
    String strName, strUsername, strEmail, strPhone;
    TextView idUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting, container, false);

        sharedPreferences = getContext().getSharedPreferences(myPreferences, Context.MODE_PRIVATE);

        name = (EditText) view.findViewById(R.id.edtName);
        email = (EditText) view.findViewById(R.id.edtEmail);
        phone = (EditText) view.findViewById(R.id.edtPhone);
        userName = (EditText) view.findViewById(R.id.edtUsername);
        userName.setText(sharedPreferences.getString("username",null));
        idUser = (TextView) view.findViewById(R.id.idUser);

        save = (Button)view.findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfileFunction();
            }
        });

        btnLogout = (Button)view.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.clear();
                edit.commit();
                Toast.makeText(getContext(),"Logout Berhasil",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getContext(),SingIn.class));
                getActivity().finish();
            }
        });

        profileFunction();

        return view;
    }

    private void updateProfileFunction() {
        final String[] message = new String[1];

        class UpdateProfile extends AsyncTask<String,String,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(getContext());
                dialog.setMessage("Loading data hutang");
                dialog.setCancelable(true);
                dialog.setIndeterminate(false);
                dialog.show();
                strName = name.getText().toString();
                strUsername = userName.getText().toString();
                strEmail = email.getText().toString();
                strPhone = phone.getText().toString();
            }

            @Override
            protected String doInBackground(String... strings) {

                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("username",strUsername));
                param.add(new BasicNameValuePair("name",strName));
                param.add(new BasicNameValuePair("email",strEmail));
                param.add(new BasicNameValuePair("phone",strPhone));
                param.add(new BasicNameValuePair("id",Integer.toString(sharedPreferences.getInt("idUser",0))));

                JSONObject json = jsonParser.makeHttpRequest(update_profile,"POST",param);

                try{
                    int sukses = json.getInt("success");

                    if(sukses == 1) {
                        message[0] = json.getString("message");
                    }else{
                        message[0] = json.getString("message");
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
                return message[0];
            }

            @Override
            protected void onPostExecute(String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                super.onPostExecute(s);
            }
        }

        UpdateProfile updateProfile = new UpdateProfile();
        updateProfile.execute();
    }

    private void profileFunction() {
        final String[] message = new String[1];

        class TampilProfile extends AsyncTask<String,String,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(getContext());
                dialog.setMessage("Loading data hutang");
                dialog.setCancelable(true);
                dialog.setIndeterminate(false);
                dialog.show();
            }

            @Override
            protected String doInBackground(String... strings) {

                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("id",Integer.toString(sharedPreferences.getInt("idUser",0))));

                JSONObject json = jsonParser.makeHttpRequest(get_profile,"POST",param);

                try{
                    int sukses = json.getInt("success");

                    if(sukses == 1) {
                        try{
                            final JSONObject data = json.getJSONObject("data");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try{
                                        name.setText(data.getString("name"));
                                        email.setText(data.getString("email"));
                                        phone.setText(data.getString("phone"));
                                        idUser.setText("ID : "+data.getString("id"));

                                    }catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            message[0] = json.getString("message");
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else{
                        message[0] = json.getString("message");
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
                return message[0];
            }

            @Override
            protected void onPostExecute(String s) {
                dialog.dismiss();
                super.onPostExecute(s);
            }
        }

        TampilProfile tampilProfile = new TampilProfile();
        tampilProfile.execute();
    }
}
